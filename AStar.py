''' 
AStar.py
by Andrew Wong and Jin Terada White
UWNetID: awong650, jintw@uw.edu
Student number: 1865788, 1924679

Assignment 2, Part 1, in CSE 415, Winter 2021.

This file implements a modified priority queue based on a minimum heap
and the A* Search algorithm. This algorithm will always identify the shortest
path to from a starting node s to a goal node g provided 0 < h'(s) <= h(s)
where h = heuristic evaluation function that estimates the distance
(path length) from s to the closest goal node and a goal state is reachable.
'''
VERBOSE = True  # Set to True to see progress; but it slows the search.

import sys

if sys.argv == [''] or len(sys.argv) < 2:
    import EightPuzzle as Problem
else:
    import importlib

    Problem = importlib.import_module(sys.argv[1])

#print("\nWelcome to UCS.")

COUNT = None  # Number of nodes expanded.
MAX_OPEN_LENGTH = None  # How long OPEN ever gets.
SOLUTION_PATH = None  # List of states from initial to goal,
# along lowest-cost path.
TOTAL_COST = None  # Sum of edge costs along the lowest-cost path.
BACKLINKS = {}  # Predecessor links, used to recover the path.
h = Problem.h  # Heuristic function

f = {}  # We will use a global hash table to associate f values with states.

# The value g(s) represents the cost along the best path found so far
# from the initial state to state s.
g = {}  # We will use a global hash table to associate g values with states.


class My_Priority_Queue:
    def __init__(self):
        self.q = []  # Actual data goes in a list.

    def __contains__(self, elt):
        '''If there is a (state, priority) pair on the list
        where state==elt, then return True.'''
        # print("In My_Priority_Queue.__contains__: elt= ", str(elt))
        for pair in self.q:
            if pair[0] == elt: return True
        return False

    def delete_min(self):
        ''' Standard priority-queue dequeuing method.'''
        if self.q == []: return []  # Simpler than raising an exception.
        temp_min_pair = self.q[0]
        temp_min_value = temp_min_pair[1]
        temp_min_position = 0
        for j in range(1, len(self.q)):
            if self.q[j][1] < temp_min_value:
                temp_min_pair = self.q[j]
                temp_min_value = temp_min_pair[1]
                temp_min_position = j
        del self.q[temp_min_position]
        return temp_min_pair

    def insert(self, state, priority):
        '''We do not keep the list sorted, in this implementation.'''
        # print("calling insert with state, priority: ", state, priority)

        if self[state] != -1:
            # print("Error: You're trying to insert an element"
            #       "into a My_Priority_Queue instance,")
            # print(" but there is already such an element in the queue.")
            return
        self.q.append((state, priority))

    def __len__(self):
        '''We define length of the priority queue to be the
        length of its list.'''
        return len(self.q)

    def __getitem__(self, state):
        '''This method enables Pythons right-bracket syntax.
        Here, something like  priority_val = my_queue[state]
        becomes possible. Note that the syntax is actually used
        in the insert method above:  self[state] != -1  '''
        for (S, P) in self.q:
            if S == state: return P
        return -1  # This value means not found.

    def __delitem__(self, state):
        '''This method enables Python's del operator to delete
        items from the queue.'''
        # print("In MyPriorityQueue.__delitem__: state is: ", str(state))
        for count, (S, P) in enumerate(self.q):
            if S == state:
                del self.q[count]
                return

    def __str__(self):
        "Code to create a string representation of the PQ."
        txt = "My_Priority_Queue: ["
        for (s, p) in self.q: txt += '(' + str(s) + ',' + str(p) + ') '
        txt += ']'
        return txt


def runAStar():
    '''This is an encapsulation of some setup before running
    UCS, plus running it and then printing some stats.'''
    initial_state = Problem.CREATE_INITIAL_STATE()
    # print("Initial State:")
    # print(initial_state)
    global COUNT, BACKLINKS, MAX_OPEN_LENGTH, SOLUTION_PATH
    COUNT = 0
    BACKLINKS = {}
    MAX_OPEN_LENGTH = 0
    SOLUTION_PATH = AStar(initial_state)
    # print(str(COUNT) + " states expanded.")
    # print('MAX_OPEN_LENGTH = ' + str(MAX_OPEN_LENGTH))
    # print("The CLOSED list is: ", ''.join([str(s[0])+' '  + ' ' for s in CLOSED]))
    return SOLUTION_PATH, COUNT, MAX_OPEN_LENGTH

def AStar(initial_state):
    '''Uniform Cost Search. This is the actual algorithm.'''
    global f, g, h, COUNT, BACKLINKS, MAX_OPEN_LENGTH, CLOSED, TOTAL_COST
    CLOSED = []
    BACKLINKS[initial_state] = None
    # The "Step" comments below help relate UCS's implementation to
    # those of Depth-First Search and Breadth-First Search.

    # STEP 1. For the start state s0 , compute f(s0) = g(s0) + h(s0) = h(s0)
    # and put [s0,f(s0)] on a list (priority queue) OPEN.
    OPEN = My_Priority_Queue()
    # STEP 1b. Assign g=0 to the start state.
    g[initial_state] = 0.0
    f[initial_state] = g[initial_state] + h(initial_state)
    OPEN.insert(initial_state, f[initial_state])

    # STEP 2. If OPEN is empty, output “DONE” and stop.
    while len(OPEN) > 0:
        #if VERBOSE: report(OPEN, CLOSED, COUNT)
        if len(OPEN) > MAX_OPEN_LENGTH: MAX_OPEN_LENGTH = len(OPEN)

        # STEP 3. Find and remove the item [s,p] on OPEN having lowest p.
        # Break ties arbitrarily. Put [s,p] on CLOSED. If s is a goal state:
        # Output its description (and backtrace a path), and, if h is known
        # to be admissible, halt.
        (S, P) = OPEN.delete_min()
        # print("In Step 3, returned from OPEN."
        # "delete_min with results (S,P)= ", (str(S), P))
        CLOSED.append((S, P))

        if Problem.GOAL_TEST(S):
            # print(Problem.GOAL_MESSAGE_FUNCTION(S))
            path = backtrace(S)
            # if f[S] is always kept track of in insertion and updating
            # priority no need to calculate it here
            # print('Length of solution path found: ' + str(
                # len(path) - 1) + ' edges')
            TOTAL_COST = f[S]
            # print('Total cost of solution path found: ' + str(TOTAL_COST))
            return path
        COUNT += 1

        # STEP 4. Generate the list L of [s', f(s')] pairs where the s' are
        # the successors of s and their f values are computed using f(s') =
        # g(s') + h(s'). Consider each [s', f(s')]
        L = []
        gs = g[S]  # Save the cost of getting to S in a variable.
        for op in Problem.OPERATORS:
            if op.precond(S):
                new_state = op.state_transf(S)
                edge_cost = S.edge_distance(new_state)
                new_g = gs + edge_cost
                new_f = new_g + h(new_state)
                # L.append((new_state, new_f))

                # If there is already a pair [s', q] on CLOSED
                # (for any value q):
                if new_state in [element[0] for element in CLOSED]:
                    q = dict(CLOSED)[new_state]
                    # If f(s') <= q, then remove [s',q] from CLOSED
                    # and add [s', new_f] to L
                    if new_f <= q:
                        CLOSED.remove((new_state, q))
                        L.append((new_state, new_f))
                        g[new_state] = new_g

                # Else if there is already a pair [s', q] on OPEN
                # (for any value q)
                elif OPEN.__contains__(new_state):
                    q = OPEN[new_state]
                    # If f(s') <= q, then remove [s',q] from OPEN and
                    # add [s', new_f] to L
                    if new_f <= q:
                        OPEN.__delitem__(new_state)
                        L.append((new_state, new_f))
                        g[new_state] = new_g

                # Otherwise we just add [s', new_f] to L
                else:
                    L.append((new_state, new_f))
                    g[new_state] = new_g

        # Add all items in L to OPEN and update f
        for (s, p) in L:
            OPEN.insert(s, p)
            f[s] = p
            BACKLINKS[s] = S

        # print_state_queue("OPEN", OPEN)
    # STEP 6. Go to Step 2.
    return None  # No more states on OPEN, and no goal reached.


# def print_state_queue(name, q):
#     print(name + " is now: ", end='')
#     print(str(q))


def backtrace(S):
    global BACKLINKS
    path = []
    while S:
        path.append(S)
        S = BACKLINKS[S]
    path.reverse()
    # print("Solution path: ")
    # for s in path:
        # print(s)
    return path


def report(open, closed, count):
    print("len(OPEN)=" + str(len(open)), end='; ')
    print("len(CLOSED)=" + str(len(closed)), end='; ')
    print("COUNT = " + str(count))


if __name__ == '__main__':
    runAStar()

