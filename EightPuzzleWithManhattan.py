'''EightPuzzleWithManhattan.py
by Andrew Wong and Jin Terada White
UWNetID: awong650, jintw@uw.edu
Student number: 1865788, 1924679

Assignment 2, Part 1, in CSE 415, Winter 2021.

This file augments EightPuzzle.py with heuristic information,
so that it can be used by an A* implementation.
The particular heuristic is the Manhattan Distance heuristic

'''

from EightPuzzle import *

POSITIONS = {
    0: [0,0],
    1: [0,1],
    2: [0,2],
    3: [1,0],
    4: [1,1],
    5: [1,2],
    6: [2,0],
    7: [2,1],
    8: [2,2]
}

def h(s):
    '''We return the number of tiles that are out of place, 
    ignoring the blank.'''
    manhattan = 0

    # look at each value s[row][col] from 0,0 to 2,2
    # find the true position of that value and compare it to its current position
    for row in range(3):
        for col in range(3):
            if s.b[row][col] != 0:            
                manhattan += (abs(row - POSITIONS[s.b[row][col]][0]) + 
                abs(col - POSITIONS[s.b[row][col]][1]))

    return manhattan