'''EightPuzzleWithHamming.py
by Andrew Wong and Jin Terada White
UWNetID: awong650, jintw@uw.edu
Student number: 1865788, 1924679

Assignment 2, Part 1, in CSE 415, Winter 2021.

This file augments EightPuzzle.py with heuristic information,
so that it can be used by an A* implementation.
The particular heuristic is the Hamming heuristic

'''

from EightPuzzle import *

def h(s):
    '''We return the number of tiles that are out of place, 
    ignoring the blank.'''
    hamming = 0
    expected_tile = 0

    for row in range(0, len(s.b)):
        for col in range(0, len(s.b[row])):
            if s.b[row][col] != 0 and s.b[row][col] != expected_tile:
                hamming += 1; 
            expected_tile += 1

    return hamming
